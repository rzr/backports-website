[[!meta date="2017-07-04 21:15:59 UTC"]]
	Bernhard Schmidt uploaded new packages for openvpn which fixed the
	following security problems:
	
	CVE-2017-7479
	
	    It was discovered that openvpn did not properly handle the
	    rollover of packet identifiers. This would allow an authenticated
	    remote attacker to cause a denial-of-service via application
	    crash.
	
	CVE-2017-7508
	
	    Guido Vranken discovered that openvpn did not properly handle
	    specific malformed IPv6 packets. This would allow a remote
	    attacker to cause a denial-of-service via application crash.
	
	CVE-2017-7520
	
	    Guido Vranken discovered that openvpn did not properly handle
	    clients connecting to an HTTP proxy with NTLMv2
	    authentication. This would allow a remote attacker to cause a
	    denial-of-service via application crash, or potentially leak
	    sensitive information like the user's proxy password.
	
	CVE-2017-7521
	
	    Guido Vranken discovered that openvpn did not properly handle
	    some x509 extensions. This would allow a remote attacker to cause
	    a denial-of-service via application crash.
	
	For the jessie-backports distribution the problems have been fixed in
	version 2.4.0-6+deb9u1~bpo8+1.
